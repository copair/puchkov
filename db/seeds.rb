# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

## Users
unless User.find_by_email('antonelo@example.com')
 User.create(first_name: 'Antonio', last_name: 'Serna', username: 'tonio',
  password: '123456', email: 'antonelo@example.com', role: 'admin')
end
unless User.find_by_email('flaco@example')
  User.create(first_name: 'Flaco', last_name: 'Zacarias', username: 'flacozaca',
    password: '123456', email: 'flaco@example.com', role: 'admin')
end

unless User.find_by_email('admin@example')
  User.create(first_name: 'Admin', last_name: 'I am', username: 'superadmin',
    password: '123456', email: 'admin@example.com', role: 'admin')
end

unless User.find_by_email('no_admin@example')
  User.create(first_name: 'Not Admmin', last_name: 'I am', username: 'noadmin',
    password: '123456', email: 'admin@example.com')
end

## People
## *add conditional
FactoryBot.create_list(:person, 3, :partners)
FactoryBot.create_list(:person, 5, :lawyers)

Person.create(first_name_en: 'Denis',
              last_name_en: 'Puchkov',
              title_en: 'Chairman, Attorney at law',
              banner_description_en: 'Denis Puchkov is a founding partner of Puchkov&amp;Partners law firm. He specializes in criminal law, investigations, litigation and intellectual property protection.',
              description_en: '<p>Denis Puchkov started his professional legal career in law enforcement agencies followed by several years of practice as an in-house lawyer in a large company. In 2002 Mr. Puchkov took the bar exam and started his private practice as an attorney at law within Sverdlovsk Regional Bar. After six years of successful private practice Denis Puchkov founded – together with his colleague Alexey Zakharov– the Puchkov &amp; Partners law firm. ',
              main_body_en: '<p>Mr. Puchkov has over ten years of professional legal experience in criminal defense, litigation and forensics. He is consistently engaged in time-consuming and laborious cases requiring strategy and meticulous planning. For example, Denis Puchkov led the case where he and his team interviewed and prepared for the court’s hearing more than 150 witnesses with respect to the hostile takeover of a company. In other cases Mr. Puchkov and his team have interviewed and prepared for the court’s hearing more than 100 witnesses with respect to commercial disputes.</p>,
                <p>Denis Puchkov is also known for handling matters that are unprecedented, like setting up practices of criminal charges for violation of intellectual property rights and criminal charges with respect to the judge, whose decision was deliberately illegal. </p>',
              member_en: '  <h4>Denis Puchkov is a member of the following organizations:</h4>
                <ul>
              <li>Association of  Russian Lawyers</li>
              <li>International Bar Association</li>
              <li>All-Russia Association of Inventors and Innovators, Sverdlovsk Regional Council. Member of the Board</li>
              <li>Chamber of Layers of Hanti-Mansiyskiy Avtonomniy Okrug</li>
                </ul>',
              quote_en: 'Nunc mauris. Sed elit erat, blandit ut, rutrum sit amet, tincidunt in, dolor. Etiam tristique aliquam velit. Nam, luctus sed, sodales at, suscipit vitae, massa.',
              practices_en: ' <ul>
              <li>Forensic / Legal Investigations</li>
              <li>Intellectual Property</li>
              <li>White Collar Crime</li>
              <li>Litigation</li>
              <li>Private clients: Defendants / Victims</li>
                </ul>',
              education_en: '<ul>
              <li>1999 – The Ural Institute of Law under the auspices of Ministry of the Interior (Yekaterinburg, Russia). Degree in Law.</li>
              <li>2007 – Institute for Advocacy of Moscow State Academy of Law (Moscow, Russia). Specialization in criminal and civil law.</li>
              <li>PhD Program at Department for Criminal Law of The Ural State Law Academy (Yekaterinburg, Russia).</li>
              <li>2012 – Plekhanov Russian University of Economics (Moscow, Russia). Department for Finances and Accounting. Specialization in financial management.</li>
              <li>2014  - International language school, Auckand, New Zealand, English for University course - Upper Intermediate certificate.</li>
                </ul>',
              languages_en: '<ul>
              <li>English</li>
              <li>Russian</li>
               </ul>',
              type_en: 'Partner',
              email_en: 'Denis.Puchkov@example.com',
              first_name_ru:  Vydumschik::Name.first_name,
              last_name_ru:  Vydumschik::Name.surname,
              title_ru:  Vydumschik::Lorem.sentence,
              description_ru:  Vydumschik::Lorem.sentence,
              banner_description_ru:  Vydumschik::Lorem.sentence,
              main_body_ru:  Vydumschik::Lorem.sentence,
              member_ru:  Vydumschik::Lorem.sentence,
              quote_ru:  Vydumschik::Lorem.sentence,
              practices_ru:  Vydumschik::Lorem.sentence,
              education_ru:  Vydumschik::Lorem.sentence,
              languages_ru:  Vydumschik::Lorem.sentence,
              email_ru: 'партнер@партнр.com',
              type_ru: 'партнер',
              telephone: '+12345678910',
             )

## CS
FactoryBot.create_list(:case_study, 5)

FactoryBot.create(:case_study,
                  title_en: 'Eppur si munue',
                  title_ru: 'Краткое описание',
                  description_en: 'short description 1',
                  description_ru: 'Краткое описание 1',
                  category_en: 'category1',
                  category_ru: 'категория1',
                  people: [ Person.first, Person.second, Person.third, Person.last ]
                 )

FactoryBot.create(:case_study,
                  title_en: 'Eppur si munue 2',
                  title_ru: 'Краткое описание',
                  description_en: 'short description 2',
                  description_ru: 'Краткое описание 2',
                  category_en: 'category1',
                  category_ru: 'категория1',
                  people: [ Person.first, Person.second, Person.third ]
                 )

FactoryBot.create(:case_study,
                  title_en: 'Eppur si munue 3',
                  title_ru: 'Краткое описание',
                  description_en: 'short description 3',
                  description_ru: 'Краткое описание 3',
                  category_en: 'category1',
                  category_ru: 'категория1',
                  people: [ Person.first, Person.second, Person.third ]
                 )

FactoryBot.create(:case_study,
                  title_en: 'Eppur si munue 4',
                  title_ru: 'Краткое описание 4',
                  description_en: 'short description 4',
                  description_ru: 'Краткое описание 4',
                  category_en: 'category1',
                  category_ru: 'категория1',
                  people: [ Person.first, Person.last ]
                 )

FactoryBot.create(:case_study,
                  title_en: 'Eppur si munue 5',
                  title_ru: 'Краткое описание 5',
                  description_en: 'short description 5',
                  description_ru: 'Краткое описание 5',
                  category_en: 'category1',
                  category_ru: 'категория1',
                  people: [ Person.last ]
                 )

## Insights
FactoryBot.create_list(:insight, 8)

FactoryBot.create(:insight,
                  title_en: 'Eppur si munue',
                  description_en: 'short description 1',
                  category_en: 'category1',
                  title_ru: 'Краткое описание',
                  description_ru: 'Краткое описание 1',
                  category_ru: 'категория1',
                 )

FactoryBot.create(:insight,
                  title_en: 'Eppur si munue',
                  description_en: 'short description 2',
                  category_en: 'category1',
                  title_ru: 'Краткое описание',
                  description_ru: 'Краткое описание 2',
                  category_ru: 'категория1',
                 )

FactoryBot.create(:insight,
                  title_en: 'Eppur si munue',
                  description_en: 'short description 3',
                  category_en: 'category1',
                  title_ru: 'Краткое описание',
                  description_ru: 'Краткое описание 3',
                  category_ru: 'категория1',
                 )

FactoryBot.create(:insight,
                  title_en: 'Eppur si munue',
                  description_en: 'short description 4',
                  category_en: 'category1',
                  title_ru: 'Краткое описание',
                  description_ru: 'Краткое описание 4',
                  category_ru: 'категория1',
                 )

## Services
FactoryBot.create(:service,
                   people: [ Person.first, Person.last ]
                 )

FactoryBot.create(:service,
                   people: [ Person.last ]
                 )

FactoryBot.create(:service,
                   people: [ Person.second ]
                 )

FactoryBot.create(:service,
                   people: [ Person.third ]
                 )

FactoryBot.create(:service,
                   people: [ Person.first ]
                 )

FactoryBot.create(:service,
                   people: [ Person.first, Person.second ]
                 )

FactoryBot.create(:service,
                   people: [ Person.fourth ]
                 )

FactoryBot.create(:service,
                   people: [ Person.fourth, Person.last ]
                 )

FactoryBot.create(:service,
                   people: [ Person.last ]
                 )

FactoryBot.create(:service,
                   people: [ Person.last, Person.second ]
                 )


FactoryBot.create(:service,
                   people: [ Person.last ]
                 )

## Main Contents
FactoryBot.create_list(:main_content, 4)

## results
puts 'There are: '
puts "#{User.all.count} users"
puts "#{CaseStudy.all.count} case studies"
puts "#{MainContent.all.count} main_contents"
puts "#{Service.all.count} services"
puts "#{Insight.all.count} insights"
puts "#{Person.all.count} people"
puts 'db:seed complete!'

