# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171212113231) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "case_studies", force: :cascade do |t|
    t.string "title_en"
    t.string "hashtag_en"
    t.text "body_en"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.string "image_url"
    t.string "category_en"
    t.string "description_en"
    t.string "title_ru"
    t.string "description_ru"
    t.string "category_ru"
    t.text "body_ru"
    t.string "hashtag_ru"
  end

  create_table "ckeditor_assets", id: :serial, force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "type", limit: 30
    t.integer "width"
    t.integer "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "insights", force: :cascade do |t|
    t.string "title_en"
    t.string "description_en"
    t.string "category_en"
    t.text "body_en"
    t.string "hashtag_en"
    t.string "title_ru"
    t.string "description_ru"
    t.string "category_ru"
    t.text "body_ru"
    t.string "hashtag_ru"
    t.string "image"
    t.string "image_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "main_contents", force: :cascade do |t|
    t.string "title_en"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title_ru"
  end

  create_table "owners", force: :cascade do |t|
    t.integer "person_id"
    t.integer "service_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "people", force: :cascade do |t|
    t.string "profile_picture"
    t.string "banner_image"
    t.string "first_name_en"
    t.string "last_name_en"
    t.string "title_en"
    t.string "description_en"
    t.string "banner_description_en"
    t.text "main_body_en"
    t.string "member_en"
    t.string "quote_en"
    t.string "practices_en"
    t.string "education_en"
    t.string "languages_en"
    t.string "type_en"
    t.string "first_name_ru"
    t.string "last_name_ru"
    t.string "title_ru"
    t.string "description_ru"
    t.string "banner_description_ru"
    t.text "main_body_ru"
    t.string "member_ru"
    t.string "quote_ru"
    t.string "practices_ru"
    t.string "education_ru"
    t.string "languages_ru"
    t.string "type_ru"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "telephone"
    t.string "email_en"
    t.string "email_ru"
  end

  create_table "services", force: :cascade do |t|
    t.string "category_en"
    t.string "image"
    t.string "image_url"
    t.string "title_en"
    t.string "description_en"
    t.text "body_en"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title_ru"
    t.string "description_ru"
    t.string "category_ru"
    t.string "body_ru"
  end

  create_table "teams", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "case_study_id"
    t.integer "person_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "username"
    t.string "userable_type"
    t.bigint "userable_id"
    t.string "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["userable_type", "userable_id"], name: "index_users_on_userable_type_and_userable_id"
  end

end
