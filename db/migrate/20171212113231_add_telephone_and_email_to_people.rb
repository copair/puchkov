class AddTelephoneAndEmailToPeople < ActiveRecord::Migration[5.1]
  def change
    add_column :people, :telephone, :string
    add_column :people, :email_en, :string
    add_column :people, :email_ru, :string
  end
end
