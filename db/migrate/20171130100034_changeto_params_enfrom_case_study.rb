class ChangetoParamsEnfromCaseStudy < ActiveRecord::Migration[5.1]
  def change
    change_table :case_studies do |t|
      t.rename :title, :title_en
      t.rename :description, :description_en
      t.rename :category, :category_en
      t.rename :body, :body_en
      t.rename :hashtag, :hashtag_en
    end
  end
end