class ChangeContentsToCaseStudies < ActiveRecord::Migration[5.1]
  def change
    rename_table :contents, :case_studies
  end
end
