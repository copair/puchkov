class AddDescriptionToContent < ActiveRecord::Migration[5.1]
  def change
    add_column :contents, :description, :string
  end
end
