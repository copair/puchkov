class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.string :profile_picture
      t.string :banner_image
      t.string :first_name_en
      t.string :last_name_en
      t.string :title_en
      t.string :description_en
      t.string :banner_description_en
      t.text :main_body_en
      t.string :member_en
      t.string :quote_en
      t.string :practices_en
      t.string :education_en
      t.string :languages_en
      t.string :type_en
      t.string :first_name_ru
      t.string :last_name_ru
      t.string :title_ru
      t.string :description_ru
      t.string :banner_description_ru
      t.text :main_body_ru
      t.string :member_ru
      t.string :quote_ru
      t.string :practices_ru
      t.string :education_ru
      t.string :languages_ru
      t.string :type_ru

      t.timestamps
    end
  end
end
