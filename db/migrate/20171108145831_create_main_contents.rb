class CreateMainContents < ActiveRecord::Migration[5.1]
  def change
    create_table :main_contents do |t|
      t.string :title

      t.timestamps
    end
  end
end
