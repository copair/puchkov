class AddAttrRuFromCaseStudy < ActiveRecord::Migration[5.1]
  def change
    change_table :case_studies do |t|
      t.string :title_ru
      t.string :description_ru
      t.string :category_ru
      t.text :body_ru
      t.string :hashtag_ru
    end
  end
end
