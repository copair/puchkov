class RemoveSectionFromCaseStudy < ActiveRecord::Migration[5.1]
  def change
    remove_column :case_studies, :section, :string
  end
end
