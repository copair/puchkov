class CreateContents < ActiveRecord::Migration[5.1]
  def change
    create_table :contents do |t|
      t.string :title
      t.string :section
      t.string :hashtag
      t.text :body

      t.timestamps
    end
  end
end
