class AddAttrRufromService < ActiveRecord::Migration[5.1]
  def change
    change_table :services do |t|
      t.string :title_ru
      t.string :description_ru
      t.string :category_ru
      t.string :body_ru
    end
  end
end
