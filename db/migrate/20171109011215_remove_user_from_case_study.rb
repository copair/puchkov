class RemoveUserFromCaseStudy < ActiveRecord::Migration[5.1]
  def change
    remove_reference :case_studies, :user, foreign_key: true
  end
end
