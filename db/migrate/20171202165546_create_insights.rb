class CreateInsights < ActiveRecord::Migration[5.1]
  def change
    create_table :insights do |t|
      t.string :title_en
      t.string :description_en
      t.string :category_en
      t.text   :body_en
      t.string :hashtag_en
      t.string :title_ru
      t.string :description_ru
      t.string :category_ru
      t.text   :body_ru
      t.string :hashtag_ru
      t.string  :image
      t.string :image_url
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
    end
  end
end
