class CreateServices < ActiveRecord::Migration[5.1]
  def change
    create_table :services do |t|
      t.string :category
      t.string :image
      t.string :image_url
      t.string :title
      t.string :description
      t.text :body
      t.string :owner

      t.timestamps
    end
  end
end
