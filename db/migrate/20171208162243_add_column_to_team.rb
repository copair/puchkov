class AddColumnToTeam < ActiveRecord::Migration[5.1]
  def change
    add_column :teams, :case_study_id, :integer
    add_column :teams, :person_id, :integer
  end
end
