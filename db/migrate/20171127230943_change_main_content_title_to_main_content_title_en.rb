class ChangeMainContentTitleToMainContentTitleEn < ActiveRecord::Migration[5.1]
  def change
    rename_column :main_contents, :title, :title_en
  end
end
