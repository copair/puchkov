# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
* System dependencies
* Configuration
* Database creation
* Database initialization
* How to run the test suite
* Services (job queues, cache servers, search engines, etc.)
* Deployment instructions
* ...


### Sources:
ABout the Blog and posts:
https://scotch.io/tutorials/build-a-blog-with-ruby-on-rails-part-1
https://scotch.io/tutorials/build-a-blog-with-ruby-on-rails-part-2

### About turbolink problem:
#### Solutions
https://stackoverflow.com/questions/14227363/using-turbolinks-in-a-rails-link-to
https://github.com/turbolinks/turbolinks/blob/master/README.md
https://stackoverflow.com/questions/18769109/rails-4-turbo-link-prevents-jquery-scripts-from-working

#### Explanation:
http://codefor.life/turbolinks-5-rails-5-not-that-bad/
https://shakacode.gitbooks.io/react-on-rails/content/docs/additional-reading/turbolinks.html <read!

#### Best explanation
https://thoughtbot.com/upcase/videos/turbolinks

#### Official documentation:
https://github.com/turbolinks/turbolinks#running-javascript-when-a-page-loads

## About renaming table model
https://stackoverflow.com/questions/46940405/rails-5-rename-table-migration
https://stackoverflow.com/questions/11924124/

## Polymorphic Associations
https://semaphoreci.com/blog/2017/08/16/polymorphic-associations-in-rails.html
http://thelazylog.com/polymorphic-routes-in-rails/

## webpacker rails heroky deployment precompile delte public assets
https://github.com/rails/webpacker/issues/547

##I18n in rails
http://guides.rubyonrails.org/i18n.html
### locale o domains Like a boos
https://phraseapp.com/blog/posts/rails-i18n-setting-and-managing-locales/

##To do the hashtags, use helper:
https://stackoverflow.com/questions/29318332/turning-hashtags-to-links-in-place-in-rails

### How to add translator to CKEditor
https://stackoverflow.com/questions/46447102/how-to-add-translator-to-ckeditor

### Renameurl paths in rails
http://api.rubyonrails.org/classes/ActionDispatch/Routing/UrlFor.html

## Polinorm aassc
https://stackoverflow.com/questions/44865191/polymorphic-associaton-transaction-fails-in-rails-

## AWS and carrierwave & Heroku
https://stackoverflow.com/questions/37990519/carrierwave-heroku#37991265
https://devcenter.heroku.com/articles/s3
https://console.aws.amazon.com/iam/home?#/security_credential
https://github.com/dwilkie/carrierwave_direct
## Rails secrets
https://www.engineyard.com/blog/encrypted-rails-secrets-on-rails-5.1
## Model & relations, Rspec testing
https://semaphoreci.com/community/tutorials/how-to-test-rails-models-with-rspec

## rails-testing-antipatern controllers
https://semaphoreci.com/blog/2014/02/11/rails-testing-antipatterns-controllers.html

## Testing controllers with devise
https://github.com/plataformatec/devise/wiki/How-To:-Test-controllers-with-Rails-3-and-4-(and-RSpec)

## SEO
https://joanswork.com/rails-seo-and-i18n-the-basics/

## many to many relationships: people can have many case studies and case studies can have many people
## person have a team and case_study have a team. Team! many to many!
## has_many :through
http://jeffthomas.xyz/many-to-many-relationships-in-rails

## Testing strong parameters! Permit! Pivotal!
https://content.pivotal.io/blog/rails-4-testing-strong-parameters

## Selenium Capybara & Rspec with rails 5
https://www.devmynd.com/blog/setting-up-rspec-and-capybara-in-rails-5-for-testing/

## Unobtrusive JavaScript via AJAX in Rails
https://blog.codeship.com/unobtrusive-javascript-via-ajax-rails/

## Coffeescript for Rails 5 and hide / show params
https://stackoverflow.com/questions/8196492/how-to-hide-and-show-a-div-with-coffeescript-rails-3-1