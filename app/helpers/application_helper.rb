# :nodoc:
module ApplicationHelper
  def tf(object, field_name)
    object.send(:"#{field_name}_#{locale}")
  end
end
