# :nodoc
class CaseStudiesController < ApplicationController
  skip_before_action :authenticate_user!, only: %i[index show]
  before_action :find_case_study, only: %i[edit update show destroy]

  # Index action to render all case_studies
  def index
    if params[:q]
      search_term = params[:q]
      if Rails.env.production?
        @case_studies = CaseStudy.where('title_en ilike ? OR description_en ilike ? OR body_en ilike ? OR hashtag_en ilike ? OR category_en ilike ? OR title_ru ilike ? OR description_ru ilike ? OR body_ru ilike ? OR hashtag_ru ilike ? OR category_ru ilike ?',
                                        "%#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%", "%#{search_term}, %#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%", "%#{search_term}", "%#{search_term}")
        # Change ilike for Postgres to don't make it case sensitive
      else
        @case_studies = CaseStudy.where('title_en LIKE ? OR description_en LIKE ? OR body_en LIKE ? OR hashtag_en LIKE ? OR category_en LIKE ? OR title_ru LIKE ? OR description_ru LIKE ? OR body_ru LIKE ? OR hashtag_ru LIKE ? OR category_ru LIKE ?',
                                        "%#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%", "%#{search_term}, %#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%", "%#{search_term}","%#{search_term}")
        # for development and test cases.
      end
    else
      @case_studies = CaseStudy.all
    end
    @case_studies = @case_studies.order('created_at DESC').paginate(page: params[:page])
    @main_content = MainContent.first
  end

  # New action for creating case_study
  def new
    @case_study = CaseStudy.new
  end

  # Create action saves the case_study into database
  def create
    @case_study = CaseStudy.new
    if @case_study.update_attributes(case_study_params)
      flash[:notice] = 'Successfully created case_study!'
      redirect_to case_study_path(@case_study)
    else
      flash[:alert] = 'Error creating new case_study!'
      render :new
    end
  end

  # Edit action retrives the case_study and renders the edit page
  def edit; end

  # Update action updates the case_study with the new information
  def update
    if @case_study.update_attributes(case_study_params)
      flash[:notice] = 'Successfully updated case_study!'
      redirect_to([@case_study])
    else
      flash[:alert] = 'Error updating case_study!'
      render :edit
    end
  end

  # The show action renders the individual case_study after retrieving the the id
  def show
    @case_studies_filter = CaseStudy.where('category_en LIKE ?', @case_study.category_en).where.not(id: @case_study.id)[0, 3] # find different as same. not countet same id!
  end

  # The destroy action removes the case_study permanently from the database
  def destroy
    if @case_study.destroy
      flash[:notice] = 'Successfully deleted case_study!'
      redirect_to case_studies_path
    else
      flash[:alert] = 'Error updating case_study!'
    end
  end

  private

  def case_study_params
    params.require(:case_study).permit(:user_id,
                                       :image,
                                       :image_url,
                                       :title_en,
                                       :hashtag_en,
                                       :body_en,
                                       :category_en,
                                       :description_en,
                                       :title_ru,
                                       :hashtag_ru,
                                       :body_ru,
                                       :category_ru,
                                       :description_ru,
                                       person_ids: [])
  end

  def find_case_study
    @case_study = CaseStudy.find(params[:id])
  end
end
