# :nodoc
class PeopleController < ApplicationController
  skip_before_action :authenticate_user!, only: %i[index show]
  before_action :find_insight, only: %i[edit update show destroy]

  # Index action to render all people
  def index
    # if params[:q]
    # search_term = params[:q]
    # if Rails.env.production?
    #   @people = Person.where('title_en ilike ? OR description_en ilike ? OR body_en ilike ? OR hashtag_en ilike ? OR category_en ilike ?',
    #                                   "%#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%", "%#{search_term}")
    #   # Change ilike for Postgres to don't make it case sensitive
    # else
    #   @people = Person.where('title_en LIKE ? OR description_en LIKE ? OR body_en LIKE ? OR hashtag_en LIKE ? OR category_en LIKE ?',
    #                                   "%#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%", "%#{search_term}")
    #   # for development and test cases.
    # end
    # else
    @people = Person.all
    @partners = Person.where(type_en: 'Partner')
    @lawyers = Person.where(type_en: 'Lawyer')
    # end
    @people = @people.order('created_at DESC').paginate(page: params[:page])
    @main_content = MainContent.all[2]
  end

  # New action for creating person
  def new
    @person = Person.new
  end

  # Create action saves the person into database
  def create
    @person = Person.new
    if @person.update_attributes(people_params)
      flash[:notice] = 'Successfully created person!'
      redirect_to insight_path(@person)
    else
      flash[:alert] = 'Error creating new person!'
      render :new
    end
  end

  # Edit action retrives the person and renders the edit page
  def edit; end

  # Update action updates the person with the new information
  def update
    if @person.update_attributes(people_params)
      flash[:notice] = 'Successfully updated person!'
      redirect_to([@person])
    else
      flash[:alert] = 'Error updating person!'
      render :edit
    end
  end

  # The show action renders the individual person after retrieving the the id
  def show
    # @people_filter = Person.where('category_en LIKE ?', @person.category_en).where.not(id: @person.id)[0, 3] # find different as same. not countet same id!
  end

  # The destroy action removes the person permanently from the database
  def destroy
    if @person.destroy
      flash[:notice] = 'Successfully deleted person!'
      redirect_to people_path
    else
      flash[:alert] = 'Error updating person!'
    end
  end

  private

  def people_params
    params.require(:person).permit(:profile_picture,
                                   :banner_image,
                                   :telephone,
                                   :first_name_en,
                                   :last_name_en,
                                   :title_en,
                                   :description_en,
                                   :banner_description_en,
                                   :main_body_en,
                                   :member_en,
                                   :quote_en,
                                   :practices_en,
                                   :education_en,
                                   :languages_en,
                                   :type_en,
                                   :type_ru,
                                   :email_en,
                                   :first_name_ru,
                                   :last_name_ru,
                                   :title_ru,
                                   :description_ru,
                                   :banner_description_ru,
                                   :main_body_ru,
                                   :member_ru,
                                   :quote_ru,
                                   :practices_ru,
                                   :education_ru,
                                   :languages_ru,
                                   :email_ru,)
  end

  def find_insight
    @person = Person.find(params[:id])
  end
end
