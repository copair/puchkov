# :nodoc
class UsersController < ApplicationController
  skip_before_action :authenticate_user!, only: [:home]

  def index
    @users = User.all
  end

  def show
    @user = current_user
  end

  def home
    @users = User.all
    @user = current_user
    @main_contents = MainContent.all
  end
end

private

def user_params
  params.require(:user).permit(:first_name, :last_name, :username, :role)
end
