# :nodoc
class ServicesController < ApplicationController
  skip_before_action :authenticate_user!, only: %i[index show]
  before_action :find_service, only: %i[edit update show destroy]

  # Index action to render all services
  def index
    if params[:q]
      search_term = params[:q]
      if Rails.env.production?
        @services = Service.where('title_en ilike ? OR description_en ilike ? OR body_en ilike ? OR category_en ilike ? OR title_ru ilike ? OR description_ru ilike ? OR body_ru ilike ? OR category_ru ilike ?',
                                  "%#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%", "%#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%")
        # Change ilike for Postgres to don't make it case sensitive
      else
        @services = Service.where('title_en LIKE ? OR description_en LIKE ? OR body_en LIKE ? OR category_en LIKE ? OR title_ru LIKE ? OR description_ru LIKE ? OR body_ru LIKE ? OR category_ru LIKE ?',
                                  "%#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%", "%#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%")
        # for development and test cases.
      end
    else
      @services = Service.all
    end
    @services = @services.order('created_at DESC').paginate(page: params[:page])
    @main_contents = MainContent.all
  end

  # New action for creating service
  def new
    @service = Service.new
  end

  # Create action saves the service into database
  def create
    @service = Service.new
    # @service.save(service_params)?
    if @service.update_attributes(service_params)
      flash[:notice] = 'Successfully created service!'
      redirect_to service_path(@service)
    else
      flash[:alert] = 'Error creating new service!'
      render :new
    end
  end

  # Edit action retrives the service and renders the edit page
  def edit; end

  # Update action updates the service with the new information
  def update
    if @service.update_attributes(service_params)
      flash[:notice] = 'Successfully updated service!'
      redirect_to([@service])
    else
      flash[:alert] = 'Error updating service!'
      render :edit
    end
  end

  # The show action renders the individual service after retrieving the the id
  def show
    @services = Service.all
  end

  # The destroy action removes the service permanently from the database
  def destroy
    if @service.destroy
      flash[:notice] = 'Successfully deleted service!'
      redirect_to services_path
    else
      flash[:alert] = 'Error updating service!'
    end
  end

  private

  def service_params
    params.require(:service).permit(:image,
                                    :image_url,
                                    :title_en,
                                    :body_en,
                                    :category_en,
                                    :description_en,
                                    :title_ru,
                                    :body_ru,
                                    :category_ru,
                                    :description_ru,
                                    :user_id,
                                    person_ids: []) # do I need user? Test! Test it!
  end

  def find_service
    @service = Service.find(params[:id])
  end
end
