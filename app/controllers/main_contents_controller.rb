# :nodoc
class MainContentsController < ApplicationController
  before_action :find_main_content, only: %i[edit update show destroy]

  # Index action to render all main_contents
  def index
    @main_contents = MainContent.all
    @main_contents = @main_contents.order('created_at DESC').paginate(page: params[:page])
  end

  # New action for creating main_content
  def new
    user = User.find(current_user.id)
    # this action is not available rigth now
  end

  # Create action saves the main_content into database
  def create
    @main_content = MainContent.new
    if @main_content.save(main_content_params)
      flash[:notice] = 'Successfully created main_content!'
      redirect_to user_main_content_path(@main_content.user, @main_content)
    else
      flash[:alert] = 'Error creating new main_content!'
      render :new
    end
  end

  # Edit action retrives the main_content and renders the edit page
  def edit; end

  # Update action updates the main_content with the new information
  def update
    if @main_content.update_attributes(main_content_params)
      flash[:notice] = 'Successfully updated main_content!'
      redirect_to([@main_content])
    else
      flash[:alert] = 'Error updating main_content!'
      render :edit
    end
  end

  # The show action renders the individual main_content after retrieving the the id
  def show; end

  # The destroy action removes the main_content permanently from the database
  def destroy
    if @main_content.destroy
      flash[:notice] = 'Successfully deleted main_content!'
      redirect_to main_contents_path
    else
      flash[:alert] = 'Error updating main_content!'
    end
  end

  private

  def main_content_params
    params.require(:main_content).permit(:title_en, :title_ru)
  end

  def find_main_content
    @main_content = MainContent.find(params[:id])
  end
end
