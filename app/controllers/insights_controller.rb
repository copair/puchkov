# :nodoc
class InsightsController < ApplicationController
  skip_before_action :authenticate_user!, only: %i[index show]
  before_action :find_insight, only: %i[edit update show destroy]

  # Index action to render all insights
  def index
    if params[:q]
      search_term = params[:q]
      if Rails.env.production?
        @insights = Insight.where('title_en ilike ? OR description_en ilike ? OR body_en ilike ? OR hashtag_en ilike ? OR category_en ilike ? OR title_ru ilike ? OR description_ru ilike ? OR body_ru ilike ? OR hashtag_ru ilike ? OR category_ru ilike ?',
                                  "%#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%", "%#{search_term}", "%#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%", "%#{search_term}")
        # Change ilike for Postgres to don't make it case sensitive
      else
        @insights = Insight.where('title_en LIKE ? OR description_en LIKE ? OR body_en LIKE ? OR hashtag_en LIKE ? OR category_en LIKE ? OR title_ru LIKE ? OR description_ru LIKE ? OR body_ru LIKE ? OR hashtag_ru LIKE ? OR category_ru LIKE ?',
                                  "%#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%", "%#{search_term}", "%#{search_term}%", '%{search_term}', '%{search_term}', "%#{search_term}%", "%#{search_term}" )
        # for development and test cases.
      end
    else
      @insights = Insight.all
    end
    @insights = @insights.order('created_at DESC').paginate(page: params[:page])
    @main_content = MainContent.all[2]
  end

  # New action for creating insight
  def new
    @insight = Insight.new
  end

  # Create action saves the insight into database
  def create
    @insight = Insight.new
    if @insight.update_attributes(insight_params)
      flash[:notice] = 'Successfully created insight!'
      redirect_to insight_path(@insight)
    else
      flash[:alert] = 'Error creating new insight!'
      render :new
    end
  end

  # Edit action retrives the insight and renders the edit page
  def edit; end

  # Update action updates the insight with the new information
  def update
    if @insight.update_attributes(insight_params)
      flash[:notice] = 'Successfully updated insight!'
      redirect_to([@insight])
    else
      flash[:alert] = 'Error updating insight!'
      render :edit
    end
  end

  # The show action renders the individual insight after retrieving the the id
  def show
    @insights_filter = Insight.where('category_en LIKE ?', @insight.category_en).where.not(id: @insight.id)[0, 3] # find different as same. not countet same id!
  end

  # The destroy action removes the insight permanently from the database
  def destroy
    if @insight.destroy
      flash[:notice] = 'Successfully deleted insight!'
      redirect_to insights_path
    else
      flash[:alert] = 'Error updating insight!'
    end
  end

  private

  def insight_params
    params.require(:insight).permit(:user_id,
                                    :image,
                                    :image_url,
                                    :title_en,
                                    :hashtag_en,
                                    :body_en,
                                    :category_en,
                                    :description_en,
                                    :title_ru,
                                    :hashtag_ru,
                                    :body_ru,
                                    :category_ru,
                                    :description_ru)
  end

  def find_insight
    @insight = Insight.find(params[:id])
  end
end
