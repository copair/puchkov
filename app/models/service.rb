# :nodoc
class Service < ApplicationRecord
  has_many :owners
  has_many :people, through: :owners

  with_options if: :title_en_present do |en_set|
    en_set.validates :title_en, presence: true
    en_set.validates :description_en, presence: true
    en_set.validates :category_en, presence: true
    en_set.validates :body_en, presence: true
  end

  with_options if: :title_ru_present do |ru_set|
    ru_set.validates :title_ru, presence: true
    ru_set.validates :description_ru, presence: true
    ru_set.validates :category_ru, presence: true
    ru_set.validates :body_ru, presence: true
  end

  mount_uploader :image, ImageUploader
  self.per_page = 12
end