# :nodoc
class Person < ApplicationRecord
  has_many :teams
  has_many :owners
  has_many :services, through: :owners
  has_many :case_studies, through: :teams

  validates :title_en, presence: true
  validates :description_en, presence: true
  validates :main_body_en, presence: true
  validates :first_name_en, presence: true
  validates :last_name_en, presence: true
  validates :banner_description_en, presence: true
  validates :main_body_en, presence: true
  validates :member_en, presence: true
  validates :quote_en, presence: true
  validates :practices_en, presence: true
  validates :education_en, presence: true
  validates :languages_en, presence: true
  validates :email_en, presence: true
  validates :type_en, presence: true

  validates :title_ru, presence: true
  validates :description_ru, presence: true
  validates :main_body_ru, presence: true
  validates :last_name_ru, presence: true
  validates :first_name_ru, presence: true
  validates :banner_description_ru, presence: true
  validates :main_body_ru, presence: true
  validates :member_ru, presence: true
  validates :quote_ru, presence: true
  validates :practices_ru, presence: true
  validates :education_ru, presence: true
  validates :languages_ru, presence: true
  validates :email_ru, presence: true
  validates :type_ru, presence: true


  validates :telephone, presence: true
  mount_uploader :profile_picture, ImageUploader
  mount_uploader :banner_image, ImageUploader
  # self.per_page = 9
end
