# :nodoc
class MainContent < ApplicationRecord
  validates :title_en, presence: true
  validates :title_ru, presence: true
  self.per_page = 9
end
