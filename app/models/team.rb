# :nodoc
class Team < ApplicationRecord
  belongs_to :case_study
  belongs_to :person
end
