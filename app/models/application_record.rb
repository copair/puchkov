# :nodoc
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def title_ru_present
     self.title_ru.present?
  end

  def title_en_present
     self.title_en.present?
  end
end
