Rails.application.routes.draw do
  get '/:locale', to: 'users#home'
  root            to: 'users#home'

  scope ':locale', locale: /#{I18n.available_locales.join("|")}/ do
    mount Ckeditor::Engine => '/ckeditor'
    get 'users/home' => 'users#home'
    get 'users/case_studies' => 'case_studies#index' # make link for edit index and probably add also some users case
    get 'users/services' => 'services#index'
    # devise_for :users
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

    devise_for :users do
      get 'users/:id' => 'users#show'
      get 'users' => 'users#index'
    end

    resources :case_studies
    resources :main_contents
    resources :services
    resources :insights
    resources :people

    resources :users, only: %i[index show]
  end
end
