require 'rails_helper'
RSpec.describe ApplicationHelper, type: :helper do
  describe 'traslate field #tf' do
    it 'traslates the object and field_name to English' do
      def locale
        :en
      end

      main_content = MainContent.new(title_en: 'Home', title_ru: 'Главная')
      en_translation = tf(main_content, :title)

      expect(en_translation).to eq('Home')
    end

    it 'traslates the object and field_name to Russian' do
      def locale
        :ru
      end

      main_content = MainContent.new(title_en: 'Home', title_ru: 'Главная')
      en_translation = tf(main_content, :title)

      expect(en_translation).to eq('Главная')
    end
  end
end
