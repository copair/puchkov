require 'faker'
FactoryBot.define do
  factory :main_content do
    title_en        { Faker::Hipster.sentences(1, true) }
    title_ru        { Vydumschik::Lorem.sentence }
  end
end
