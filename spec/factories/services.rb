FactoryBot.define do
  factory :service do
    title_en        { Faker::Hipster.sentences(1, true) }
    description_en  { Faker::Hipster.sentences(1, true) }
    category_en     { Faker::Hipster.word }
    body_en         { Faker::Hipster.paragraph(4, true, 8) }
    title_ru        { Vydumschik::Lorem.sentence }
    description_ru  { Vydumschik::Lorem.sentence }
    category_ru     { Vydumschik::Lorem.word }
    body_ru         { Vydumschik::Lorem.sentence }
  end
end
