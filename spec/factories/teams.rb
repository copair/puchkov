FactoryBot.define do
  factory :team do
    case_study_id { CaseStudy.last }
    person_id     { Person.last }
  end
end
