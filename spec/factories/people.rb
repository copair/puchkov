FactoryBot.define do
  factory :person do
    first_name_en           { Faker::Name.first_name }
    last_name_en            { Faker::Name.last_name }
    title_en                { Faker::Hipster.sentences(1, true) }
    description_en          { Faker::Hipster.sentences(1, true) }
    banner_description_en   { Faker::Hipster.sentences(1, true) }
    main_body_en            { Faker::Hipster.paragraph(4, true, 8) }
    member_en               'Member of Law'
    quote_en                { Faker::Hipster.sentences(1, true) }
    practices_en            { Faker::Hipster.sentences(1, true) }
    education_en            { Faker::Hipster.sentences(1, true) }
    languages_en            'Russian, English'
    email_en                { Faker::Internet.email }
    first_name_ru           { Vydumschik::Name.first_name }
    last_name_ru            { Vydumschik::Name.surname }
    title_ru                { Vydumschik::Lorem.sentence }
    description_ru          { Vydumschik::Lorem.sentence }
    banner_description_ru   { Vydumschik::Lorem.sentence }
    main_body_ru            { Vydumschik::Lorem.sentence }
    member_ru               { Vydumschik::Lorem.word }
    quote_ru                { Vydumschik::Lorem.sentence }
    practices_ru            { Vydumschik::Lorem.sentence }
    education_ru            { Vydumschik::Lorem.sentence }
    languages_ru            { Vydumschik::Lorem.word }
    email_ru                { "#{Vydumschik::Name.first_name}@#{Vydumschik::Lorem.word}.kom" }
    telephone               { Faker::PhoneNumber.cell_phone }
    trait :partners do
      type_en 'Partner'
      type_ru 'партнер'
    end

    trait :lawyers do
      type_en 'Lawyer'
      type_ru 'Адвокат'
    end
  end
end
