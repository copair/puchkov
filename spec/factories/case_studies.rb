require 'faker'
FactoryBot.define do
  factory :case_study do
    title_en        { Faker::Hipster.sentences(1, true) }
    description_en  { Faker::Hipster.sentences(1, true) }
    category_en     { Faker::Hipster.word }
    body_en         { Faker::Hipster.paragraph(4, true, 8) }
    hashtag_en      { Faker::Hipster.words(4) }
    title_ru        { Vydumschik::Lorem.sentence }
    description_ru  { Vydumschik::Lorem.sentence }
    category_ru     { Vydumschik::Lorem.word }
    body_ru         { Vydumschik::Lorem.sentence }
    hashtag_ru      { Vydumschik::Lorem.word }

    # trait :with_preset_user do ## trait failing check why
    #   user User.first
    # end
  end
end
