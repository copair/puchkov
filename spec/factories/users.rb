require 'faker'
FactoryBot.define do
  factory :user do
    first_name            { Faker::Name.first_name }
    last_name             { Faker::Name.last_name }
    username              { Faker::Internet.unique.user_name(%w[. _ -]) }
    email                 { Faker::Internet.unique.email }
    password              '123456'
    password_confirmation '123456'
    role                  'user'

    trait :admin do
      role 'admin'
    end
  end
end
