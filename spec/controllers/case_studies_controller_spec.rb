require 'rails_helper'

RSpec.describe CaseStudiesController, type: :controller do
  login_admin

  it 'has a current_user' do
    # note the fact that you should remove the "validate_session" parameter if this was a scaffold-generated controller
    expect(subject.current_user).not_to eq(nil)
  end

  context 'as an admin' do
    login_admin

    describe 'GET #index' do
      it 'returns http success' do
        get :index, params: { locale: 'en' }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #new' do
      it 'returns http success' do
        get :new, params: { locale: 'en' }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #show' do
      it 'returns http success' do
        case_study = FactoryBot.create(:case_study)
        get :show, params: { locale: 'en', id: case_study.id }
        expect(response).to have_http_status(:success)
        case_study.delete
      end
    end

    describe 'GET #destroy' do
      it 'PENDING! returns http success' do
        case_study = FactoryBot.create(:case_study)
        get :destroy, params: { locale: 'en', id: case_study.id }
        expect(response).to have_http_status(302)
        case_study.delete
      end
    end

    describe 'GET #edit' do
      it 'returns http success' do
        case_study = FactoryBot.create(:case_study)
        get :edit, params: { locale: 'en', id: case_study.id }
        expect(response).to have_http_status(:success)
        case_study.delete
      end
    end

    describe 'GET #create' do
      it 'returns http success' do
        post :create, params: { locale: 'en', case_study: FactoryBot.attributes_for(:case_study) }
        expect(response).to have_http_status(302)
      end
    end

    describe 'GET #update' do
      it 'returns http success' do
        case_study = FactoryBot.create(:case_study)
        post :update, params: { locale: 'en', id: case_study.id, case_study: FactoryBot.attributes_for(:case_study) }
        expect(response).to have_http_status(302)
        case_study.delete
      end
    end
  end
end
