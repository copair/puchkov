require 'rails_helper'

RSpec.describe PeopleController, type: :controller do
  login_admin

  it 'has a current_user' do
    # note the fact that you should remove the "validate_session" parameter if this was a scaffold-generated controller
    expect(subject.current_user).not_to eq(nil)
  end

  context 'as an admin' do
    login_admin

    describe 'GET #index' do
      it 'returns http success' do
        get :index, params: { locale: 'en' }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #new' do
      it 'returns http success' do
        get :new, params: { locale: 'en' }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #show' do
      it 'returns http success' do
        person = FactoryBot.create(:person, :partners)
        get :show, params: { locale: 'en', id: person.id }
        expect(response).to have_http_status(:success)
        person.delete
      end
    end

    describe 'GET #destroy' do
      it 'PENDING! returns http success' do
        person = FactoryBot.create(:person, :partners)
        get :destroy, params: { locale: 'en', id: person.id }
        expect(response).to have_http_status(302)
        person.delete
      end
    end

    describe 'GET #edit' do
      it 'returns http success' do
        person = FactoryBot.create(:person, :partners)
        get :edit, params: { locale: 'en', id: person.id }
        expect(response).to have_http_status(:success)
        person.delete
      end
    end

    describe 'GET #create' do
      it 'returns http success' do
        post :create, params: { locale: 'en', person: FactoryBot.attributes_for(:person) }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #update' do
      it 'returns http success' do
        person = FactoryBot.create(:person, :partners)
        post :update, params: { locale: 'en', id: person.id, person: FactoryBot.attributes_for(:person) }
        expect(response).to have_http_status(302)
        person.delete
      end
    end
  end
end
