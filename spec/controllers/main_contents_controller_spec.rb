require 'rails_helper'

RSpec.describe MainContentsController, type: :controller do
  login_admin

  it 'has a current_user' do
    # note the fact that you should remove the "validate_session" parameter if this was a scaffold-generated controller
    expect(subject.current_user).not_to eq(nil)
  end

  context 'as an admin' do
    login_admin

    describe 'GET #index' do
      it 'returns http success' do
        get :index, params: { locale: 'en' }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #new' do
      it 'returns http success' do
        get :new, params: { locale: 'en' }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #show' do
      it 'returns http success' do
        main_content = FactoryBot.create(:main_content)
        get :show, params: { locale: 'en', id: main_content.id }
        expect(response).to have_http_status(:success)
        main_content.delete
      end
    end

    describe 'GET #destroy' do
      it 'PENDING! returns http success' do
        main_content = FactoryBot.create(:main_content)
        get :destroy, params: { locale: 'en', id: main_content.id }
        expect(response).to have_http_status(302)
        main_content.delete
      end
    end

    describe 'GET #edit' do
      it 'returns http success' do
        main_content = FactoryBot.create(:main_content)
        get :edit, params: { locale: 'en', id: main_content.id }
        expect(response).to have_http_status(:success)
        main_content.delete
      end
    end

    describe 'GET #create' do
      it 'returns http success' do
        post :create, params: { locale: 'en', main_content: FactoryBot.attributes_for(:main_content) }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #update' do
      it 'returns http success' do
        main_content = FactoryBot.create(:main_content)
        post :update, params: { locale: 'en', id: main_content.id, main_content: FactoryBot.attributes_for(:main_content) }
        expect(response).to have_http_status(302)
        main_content.delete
      end
    end
  end
end
