require 'rails_helper'

RSpec.describe ServicesController, type: :controller do
  login_admin

  it 'has a current_user' do
    # note the fact that you should remove the "validate_session" parameter if this was a scaffold-generated controller
    expect(subject.current_user).not_to eq(nil)
  end

  context 'as an admin' do
    login_admin

    describe 'GET #index' do
      it 'returns http success' do
        get :index, params: { locale: 'en' }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #new' do
      it 'returns http success' do
        get :new, params: { locale: 'en' }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #show' do
      it 'returns http success' do
        service = FactoryBot.create(:service)
        get :show, params: { locale: 'en', id: service.id }
        expect(response).to have_http_status(:success)
        service.delete
      end
    end

    describe 'GET #destroy' do
      it 'PENDING! returns http success' do
        service = FactoryBot.create(:service)
        get :destroy, params: { locale: 'en', id: service.id }
        expect(response).to have_http_status(302)
        service.delete
      end
    end

    describe 'GET #edit' do
      it 'returns http success' do
        service = FactoryBot.create(:service)
        get :edit, params: { locale: 'en', id: service.id }
        expect(response).to have_http_status(:success)
        service.delete
      end
    end

    describe 'GET #create' do
      it 'returns http success' do
        post :create, params: { locale: 'en', service: FactoryBot.attributes_for(:service) }
        expect(response).to have_http_status(302)
      end
    end

    describe 'GET #update' do
      it 'returns http success' do
        service = FactoryBot.create(:service)
        post :update, params: { locale: 'en', id: service.id, service: FactoryBot.attributes_for(:service) }
        expect(response).to have_http_status(302)
        service.delete
      end
    end
  end
end
