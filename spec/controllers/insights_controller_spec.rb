require 'rails_helper'

RSpec.describe InsightsController, type: :controller do
  login_admin

  it 'has a current_user' do
    # note the fact that you should remove the "validate_session" parameter if this was a scaffold-generated controller
    expect(subject.current_user).not_to eq(nil)
  end

  context 'as an admin' do
    login_admin

    describe 'GET #index' do
      it 'returns http success' do
        get :index, params: { locale: 'en' }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #new' do
      it 'returns http success' do
        get :new, params: { locale: 'en' }
        expect(response).to have_http_status(:success)
      end
    end

    describe 'GET #show' do
      it 'returns http success' do
        insight = FactoryBot.create(:insight)
        get :show, params: { locale: 'en', id: insight.id }
        expect(response).to have_http_status(:success)
        insight.delete
      end
    end

    describe 'GET #destroy' do
      it 'PENDING! returns http success' do
        insight = FactoryBot.create(:insight)
        get :destroy, params: { locale: 'en', id: insight.id }
        expect(response).to have_http_status(302)
        insight.delete
      end
    end

    describe 'GET #edit' do
      it 'returns http success' do
        insight = FactoryBot.create(:insight)
        get :edit, params: { locale: 'en', id: insight.id }
        expect(response).to have_http_status(:success)
        insight.delete
      end
    end

    describe 'GET #create' do
      it 'returns http success' do
        post :create, params: { locale: 'en', insight: FactoryBot.attributes_for(:insight) }
        expect(response).to have_http_status(302)
      end
    end

    describe 'GET #update' do
      it 'returns http success' do
        insight = FactoryBot.create(:insight)
        post :update, params: { locale: 'en', id: insight.id, insight: FactoryBot.attributes_for(:insight) }
        expect(response).to have_http_status(302)
        insight.delete
      end
    end
  end
end
