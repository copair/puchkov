require 'rails_helper'

RSpec.describe Person do
  describe 'Associations' do
    it { is_expected.to have_many(:case_studies) }
    it { is_expected.to have_many(:teams) }
  end

  it { is_expected.to validate_presence_of(:title_en) }
  it { is_expected.to validate_presence_of(:description_en) }
  it { is_expected.to validate_presence_of(:main_body_en) }
  it { is_expected.to validate_presence_of(:first_name_en) }
  it { is_expected.to validate_presence_of(:last_name_en) }
  it { is_expected.to validate_presence_of(:banner_description_en) }
  it { is_expected.to validate_presence_of(:main_body_en) }
  it { is_expected.to validate_presence_of(:member_en) }
  it { is_expected.to validate_presence_of(:quote_en) }
  it { is_expected.to validate_presence_of(:practices_en) }
  it { is_expected.to validate_presence_of(:education_en) }
  it { is_expected.to validate_presence_of(:languages_en) }
  it { is_expected.to validate_presence_of(:email_en) }
  it { is_expected.to validate_presence_of(:title_ru) }
  it { is_expected.to validate_presence_of(:description_ru) }
  it { is_expected.to validate_presence_of(:main_body_ru) }
  it { is_expected.to validate_presence_of(:first_name_ru) }
  it { is_expected.to validate_presence_of(:last_name_ru) }
  it { is_expected.to validate_presence_of(:banner_description_ru) }
  it { is_expected.to validate_presence_of(:main_body_ru) }
  it { is_expected.to validate_presence_of(:member_ru) }
  it { is_expected.to validate_presence_of(:quote_ru) }
  it { is_expected.to validate_presence_of(:practices_ru) }
  it { is_expected.to validate_presence_of(:education_ru) }
  it { is_expected.to validate_presence_of(:languages_ru) }
  it { is_expected.to validate_presence_of(:email_ru) }
  it { is_expected.to validate_presence_of(:telephone) }

  let(:person) { FactoryBot.create(:person, :partners) }

  specify 'model attributes' do
    expect(person).to be_valid
    expect(person).to respond_to :profile_picture
    expect(person).to respond_to :banner_image
    expect(person).to respond_to :type_en
    expect(person).to respond_to :type_ru
    expect(described_class.count).to eq(1)
  end
end
