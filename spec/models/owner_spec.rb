require 'rails_helper'

RSpec.describe Owner do
  describe 'Associations' do
    it { is_expected.to belong_to(:service) }
    it { is_expected.to belong_to(:person) }
  end
end
