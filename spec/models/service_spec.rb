require 'rails_helper'

RSpec.describe Service do

  context 'when title_en is present' do
    before { allow(subject).to receive(:title_en_present).and_return(true) }

    it { is_expected.to validate_presence_of(:title_en) }
    it { is_expected.to validate_presence_of(:category_en) }
    it { is_expected.to validate_presence_of(:description_en) }
    it { is_expected.to validate_presence_of(:body_en) }
  end

  context 'when title_ru is present' do
    before { allow(subject).to receive(:title_ru_present).and_return(true) }

    it { is_expected.to validate_presence_of(:title_ru) }
    it { is_expected.to validate_presence_of(:category_ru) }
    it { is_expected.to validate_presence_of(:description_ru) }
    it { is_expected.to validate_presence_of(:body_ru) }
  end

  let(:service) { FactoryBot.create(:service) }

  specify 'model attributes' do
    expect(service).to be_valid
    expect(service).to respond_to :image
    expect(service).to respond_to :image_url
    expect(service).to respond_to :person_ids

    expect(described_class.count).to eq(1)
  end
end
