require 'rails_helper'

RSpec.describe Insight do

  context 'when title_en is present' do
    before { allow(subject).to receive(:title_en_present).and_return(true) }

    it { is_expected.to validate_presence_of(:title_en) }
    it { is_expected.to validate_presence_of(:category_en) }
    it { is_expected.to validate_presence_of(:description_en) }
    it { is_expected.to validate_presence_of(:body_en) }
  end

  context 'when title_ru is present' do
    before { allow(subject).to receive(:title_ru_present).and_return(true) }

    it { is_expected.to validate_presence_of(:title_ru) }
    it { is_expected.to validate_presence_of(:category_ru) }
    it { is_expected.to validate_presence_of(:description_ru) }
    it { is_expected.to validate_presence_of(:body_ru) }
  end

  let(:insight) { FactoryBot.create(:insight) }

  specify 'model attributes' do
    expect(insight).to be_valid
    expect(insight).to respond_to :hashtag_ru
    expect(insight).to respond_to :hashtag_en
    expect(insight).to respond_to :image
    expect(insight).to respond_to :image_url
    expect(described_class.count).to eq(1)
  end
end
