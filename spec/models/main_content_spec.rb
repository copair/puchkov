require 'rails_helper'

RSpec.describe MainContent do
  it { is_expected.to validate_presence_of(:title_en) }
  it { is_expected.to validate_presence_of(:title_ru) }

  let(:main_content) { FactoryBot.create(:main_content) }

  specify 'model attributes' do
    expect(main_content).to be_valid
    expect(main_content).to respond_to :title_en
    expect(main_content).to respond_to :title_ru
    expect(described_class.count).to eq(1)
  end
end
