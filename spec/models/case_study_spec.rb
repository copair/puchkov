require 'rails_helper'

RSpec.describe CaseStudy do
  describe 'Associations' do
    it { is_expected.to have_many(:people) }
    it { is_expected.to have_many(:teams) }
  end

  context 'when title_en is present' do
    before { allow(subject).to receive(:title_en_present).and_return(true) }

    it { is_expected.to validate_presence_of(:title_en) }
    it { is_expected.to validate_presence_of(:category_en) }
    it { is_expected.to validate_presence_of(:description_en) }
    it { is_expected.to validate_presence_of(:body_en) }
  end

  context 'when title_ru is present' do
    before { allow(subject).to receive(:title_ru_present).and_return(true) }

    it { is_expected.to validate_presence_of(:title_ru) }
    it { is_expected.to validate_presence_of(:category_ru) }
    it { is_expected.to validate_presence_of(:description_ru) }
    it { is_expected.to validate_presence_of(:body_ru) }
  end

  let(:case_study) { FactoryBot.create(:case_study) }

  specify 'model attributes' do
    expect(case_study).to be_valid
    expect(case_study).to respond_to :hashtag_ru
    expect(case_study).to respond_to :hashtag_en
    expect(case_study).to respond_to :image
    expect(case_study).to respond_to :image_url
    expect(case_study).to respond_to :person_ids

    expect(described_class.count).to eq(1)
  end
end
