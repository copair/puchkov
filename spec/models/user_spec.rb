require 'rails_helper'

RSpec.describe User do
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_presence_of(:username) }
  it { is_expected.to validate_presence_of(:password) }
  it { is_expected.to validate_presence_of(:first_name) }
  it { is_expected.to validate_presence_of(:last_name) }

  let(:user) { FactoryBot.create(:user) }

  specify 'model attributes' do
    expect(user).to be_valid
    expect(user).to respond_to :role
    expect(described_class.count).to eq(1)
  end

  it 'is a short password' do
    with_short_password = FactoryBot.build(:user, password: '123')

    expect(with_short_password).not_to be_valid
  end

  it 'is username unique' do
    with_an_unique_username = FactoryBot.create(:user, username: 'lolito')

    expect(with_an_unique_username).to be_valid
    expect { FactoryBot.create(:user, username: 'lolito') }.to raise_error(ActiveRecord::RecordInvalid)
  end

  it 'is email unique' do
    with_an_unique_email = FactoryBot.create(:user, email: 'lolito@example.com')

    expect(with_an_unique_email).to be_valid
    expect { FactoryBot.create(:user, email: 'lolito@example.com') }.to raise_error(ActiveRecord::RecordInvalid)
  end
end
