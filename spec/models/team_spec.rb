require 'rails_helper'

RSpec.describe Team do
  # let(:team) { FactoryBot.create(:team) }

  # specify "model attributes" do
  #   expect(team).to be_valid
  #   expect(team).to respond_to :case_study_id
  #   expect(team).to respond_to :person_id
  #   expect(described_class.count).to eq(1)
  # end

  describe 'Associations' do
    it { is_expected.to belong_to(:case_study) }
    it { is_expected.to belong_to(:person) }
  end
end
