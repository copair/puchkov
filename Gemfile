source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.4'
# Use postgresql as the database for Active Record & Heroku
gem 'pg'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Use SCSS for stylesheets
gem 'bootstrap-sass', '~> 3.3'
gem 'sass-rails', '~> 5.0'
gem 'simple_form', '~> 3.2'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby
gem 'devise-i18n'
# Add heroku dependency
gem 'rmagick'
# pagination
gem 'will_paginate', '~> 3.1.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development
gem 'webpacker'
# Use Cancancan for authorization library
gem 'cancancan', '~> 2.0'
# Use devise as authentication solution
gem 'devise'
# For aws must do use
# gem 'aws-sdk', '~> 2'
gem 'carrierwave_direct'
# Use carrierwave as a simple and extremely flexible way to upload files
gem 'carrierwave', '~> 1.0'
gem 'mini_magick'
#  editor for the creation and updating of a post
gem 'ckeditor', '~> 4.1'
# generate fake data
gem 'faker'
# Russian faker
gem 'vydumschik', '~> 0.2.0'

# Use Factory Girl for generating random test data
gem 'factory_bot_rails'
# or
gem 'factory_bot'
# help finding missing I18n translations https://github.com/glebm/i18n-tasks
# gem 'i18n-tasks', '~> 0.9.19'

group :test do
  gem 'shoulda-matchers', '~> 3.0'
  gem 'simplecov', require: false
end

group :development, :test do
  # byebug to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '~> 2.13'
  gem 'rspec-rails'
  gem 'selenium-webdriver'
  # Setup Rails from TDD via:
  # http://asquera.de/blog/2016-03-31/tdd-with-guard/
  gem 'dotenv-rails'
  gem 'guard'
  gem 'guard-bundler', require: false
  gem 'guard-rspec', require: false
  gem 'guard-rubocop', require: false
  gem 'rspec'
  gem 'rubocop'
  gem 'rubocop-rspec'
end

group :development do
  # Access an IRB console on exception <%= console %> anywhere in the code.
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application
  # running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

# Ruby version for tis project:
ruby '2.4.1'
